import { expect, haveResource } from '@aws-cdk/assert'
import * as sst from '@serverless-stack/resources'
import CounterStack from './CounterStack'

test('has a lambda resource', () => {
  const app = new sst.App()

  const stack = new CounterStack(app, 'counter-stack')

  expect(stack).to(haveResource('AWS::Lambda::Function'))
})
