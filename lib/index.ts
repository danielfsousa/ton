import cdk from '@aws-cdk/core'
import lambda from '@aws-cdk/aws-lambda'
import sst from '@serverless-stack/resources'
import CounterStack from './CounterStack'
import AuthStack from './AuthStack'
import * as pkg from '../package.json'

export default function main(app: sst.App): void {
  app.setDefaultFunctionProps({
    memorySize: 512,
    timeout: cdk.Duration.seconds(3),
    runtime: lambda.Runtime.NODEJS_14_X,
    tracing: lambda.Tracing.ACTIVE,
    environment: {
      NODE_OPTIONS: '--enable-source-maps',
      STAGE: app.stage,
    },
  })

  const counterStack = new CounterStack(app, 'counter-stack')
  const authStack = new AuthStack(app, 'auth-stack')

  addTagsToStacks(app.stage, counterStack, authStack)
}

function addTagsToStacks(stage: string, ...stacks: sst.Stack[]): void {
  for (const stack of stacks) {
    cdk.Tags.of(stack).add('Environment', stage)
    cdk.Tags.of(stack).add('Version', pkg.version)
    cdk.Tags.of(stack).add('ApplicationId', 'ton')
    cdk.Tags.of(stack).add('ApplicationRole', 'api')
  }
}
