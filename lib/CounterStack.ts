import * as sst from '@serverless-stack/resources'

export default class CounterStack extends sst.Stack {
  constructor(scope: sst.App, id: string, props?: sst.StackProps) {
    super(scope, id, props)

    const api = new sst.Api(this, 'CounterApi', {
      routes: {
        'GET  /counter': 'src/counter/get-counter-fn.handler',
        'POST /counter': 'src/counter/increment-counter-fn.handler',
      },
    })

    this.addOutputs({
      ApiEndpoint: api.url,
    })
  }
}
