import iam from '@aws-cdk/aws-iam'
import dynamodb from '@aws-cdk/aws-dynamodb'
import cognito from '@aws-cdk/aws-cognito'
import apigAuthorizers from '@aws-cdk/aws-apigatewayv2-authorizers'
import lambda from '@aws-cdk/aws-lambda'
import sst from '@serverless-stack/resources'

export interface AddCognitoTriggerProps {
  operation: cognito.UserPoolOperation
  userPool: cognito.UserPool
  fn: sst.Function
}

export interface AttachPermissionsOptions {
  api: sst.Api
  auth: sst.Auth
  usersTable: sst.Table
}

export default class AuthStack extends sst.Stack {
  constructor(scope: sst.App, id: string, props?: sst.StackProps) {
    super(scope, id, props)

    const auth = this.createAuth()
    if (!auth.cognitoUserPool || !auth.cognitoUserPoolClient) {
      throw new Error('UserPool or UserPoolClient was not created')
    }

    const usersTable = this.createUsersTable()
    const api = this.createApi(auth, usersTable)
    const preSignUpFn = new sst.Function(this, 'PreSignUpFn', {
      handler: 'src/auth/pre-sign-up-fn.handler',
      permissions: [usersTable],
      environment: {
        USERS_TABLE_NAME: usersTable.dynamodbTable.tableName,
      },
    })

    this.attachPermissions({ api, auth, usersTable })

    this.addCognitoTrigger({
      fn: preSignUpFn,
      userPool: auth.cognitoUserPool,
      operation: cognito.UserPoolOperation.PRE_SIGN_UP,
    })

    this.addOutputs({
      ApiEndpoint: api.url,
      UsersTableName: usersTable.dynamodbTable.tableName,
      UserPoolId: auth.cognitoUserPool.userPoolId,
      IdentityPoolId: auth.cognitoCfnIdentityPool.ref,
      UserPoolClientId: auth.cognitoUserPoolClient.userPoolClientId,
    })
  }

  private createAuth(): sst.Auth {
    const auth = new sst.Auth(this, 'Auth', {
      cognito: {
        userPoolClient: {
          authFlows: {
            userSrp: true,
          },
        },
        userPool: {
          signInAliases: { email: true },
          accountRecovery: cognito.AccountRecovery.EMAIL_ONLY,
          selfSignUpEnabled: true,
          userVerification: {
            emailSubject: 'Confirme seu e-mail',
            emailBody: 'Obrigado por se registrar ao ton! O seu código de verificação é: {####}',
            emailStyle: cognito.VerificationEmailStyle.CODE,
          },
          standardAttributes: {
            fullname: {
              required: true,
              mutable: false,
            },
            address: {
              required: false,
              mutable: true,
            },
          },
          // https://pages.nist.gov/800-63-3/sp800-63b.html#memsecret
          passwordPolicy: {
            minLength: 8,
            requireLowercase: true,
            requireUppercase: true,
            requireDigits: true,
            requireSymbols: false,
          },
        },
      },
    })

    auth.cognitoUserPool?.addDomain('CognitoDomain', {
      cognitoDomain: {
        domainPrefix: `ton-${this.stage}`,
      },
    })

    return auth
  }

  private createUsersTable(): sst.Table {
    return new sst.Table(this, 'UsersTable', {
      dynamodbTable: {
        encryption: dynamodb.TableEncryption.AWS_MANAGED,
      },
      primaryIndex: { partitionKey: 'id' },
      secondaryIndexes: {
        emailCreatedAtIndex: { partitionKey: 'email', sortKey: 'createdAt' },
      },
      fields: {
        id: sst.TableFieldType.STRING,
        email: sst.TableFieldType.STRING,
        name: sst.TableFieldType.STRING,
        address: sst.TableFieldType.STRING,
        createdAt: sst.TableFieldType.STRING,
        updatedAt: sst.TableFieldType.STRING,
      },
    })
  }

  private createApi(auth: sst.Auth, usersTable: sst.Table): sst.Api {
    return new sst.Api(this, 'AuthApi', {
      defaultAuthorizationType: sst.ApiAuthorizationType.NONE,
      defaultFunctionProps: {
        environment: {
          USERS_TABLE_NAME: usersTable.dynamodbTable.tableName,
          COGNITO_USER_POOL_ID: auth.cognitoUserPool!.userPoolId,
          COGNITO_CLIENT_ID: auth.cognitoUserPoolClient!.userPoolClientId,
        },
      },
      routes: {
        'GET  /user': {
          function: 'src/auth/get-authenticated-user-fn.handler',
          authorizationType: sst.ApiAuthorizationType.JWT,
          authorizer: new apigAuthorizers.HttpUserPoolAuthorizer({
            userPool: auth.cognitoUserPool!,
            userPoolClient: auth.cognitoUserPoolClient!,
          }),
        },
        'POST /login': 'src/auth/login-fn.handler',
        'POST /signup': 'src/auth/sign-up-fn.handler',
        'POST /signup/confirm': 'src/auth/sign-up-confirm-fn.handler',
      },
    })
  }

  private attachPermissions({ api, auth, usersTable }: AttachPermissionsOptions): void {
    auth.attachPermissionsForAuthUsers([api])
    auth.attachPermissionsForUnauthUsers([api])

    api
      .getFunction('GET /user')
      ?.attachPermissions([[usersTable.dynamodbTable, usersTable.dynamodbTable.grantReadData.name]])

    api.getFunction('POST /login')?.attachPermissions([
      new iam.PolicyStatement({
        effect: iam.Effect.ALLOW,
        resources: [auth.cognitoUserPool!.userPoolArn],
        actions: ['cognito-idp:InitiateAuth'],
      }),
    ])

    api.getFunction('POST /signup')?.attachPermissions([
      new iam.PolicyStatement({
        effect: iam.Effect.ALLOW,
        resources: [auth.cognitoUserPool!.userPoolArn],
        actions: ['cognito-idp:SignUp'],
      }),
    ])

    api.getFunction('POST /signup/confirm')?.attachPermissions([
      new iam.PolicyStatement({
        effect: iam.Effect.ALLOW,
        resources: [auth.cognitoUserPool!.userPoolArn],
        actions: ['cognito-idp:ConfirmSignUp'],
      }),
    ])
  }

  private addCognitoTrigger({ fn, userPool, operation }: AddCognitoTriggerProps): void {
    userPool.addTrigger(operation, fn as lambda.IFunction)
    fn.addPermission('CognitoPermission', {
      principal: new iam.ServicePrincipal('cognito-idp.amazonaws.com'),
      sourceArn: userPool.userPoolArn,
    })
  }
}
