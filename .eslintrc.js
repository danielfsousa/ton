const path = require('path')

module.exports = {
  extends: 'standard-with-typescript',
  parserOptions: {
    project: path.join(__dirname, './tsconfig.json'),
  },
  rules: {
    'comma-dangle': 'off',
    'no-new': 'off',
    'space-before-function-paren': 'off',
    '@typescript-eslint/space-before-function-paren': 'off',
    '@typescript-eslint/strict-boolean-expressions': 'off',
    '@typescript-eslint/indent': 'off',
  },
  overrides: [
    {
      files: ['lib/**/*.ts'],
      rules: {
        '@typescript-eslint/no-non-null-assertion': 'off',
      },
    },
  ],
}
