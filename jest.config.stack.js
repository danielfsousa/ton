const path = require('path')

module.exports = {
  preset: 'ts-jest',
  coverageReporters: ['json'],
  verbose: true,
  coverageDirectory: 'coverage/stack',
  collectCoverageFrom: ['lib/**', '!src/**/*.test.ts'],
  testPathIgnorePatterns: ['/node_modules/', './build', '/src', '/test/integration', '/test/e2e'],
  testEnvironment: 'node',
  globals: {
    'ts-jest': {
      tsconfig: path.join(__dirname, './tsconfig.json'),
    },
  },
}
