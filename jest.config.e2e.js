const path = require('path')

module.exports = {
  preset: 'ts-jest',
  coverageReporters: ['json'],
  verbose: true,
  coverageDirectory: 'coverage/e2e',
  collectCoverageFrom: ['src/**', '!src/**/*.test.ts'],
  testPathIgnorePatterns: ['/node_modules/', './build', '/lib', '/src'],
  testEnvironment: 'node',
  testTimeout: 20000, // 20 seconds
  globals: {
    'ts-jest': {
      tsconfig: path.join(__dirname, './tsconfig.json'),
    },
  },
}
