const express = require('express')
const swaggerUi = require('swagger-ui-express')
const YAML = require('yamljs')

const port = 8080
const counterDoc = YAML.load('./docs/openapi-counter.yaml')

express()
  .use(swaggerUi.serve, swaggerUi.setup(counterDoc))
  .listen(port, () => console.log(`listening at http://localhost:${port}`))
