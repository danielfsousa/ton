const path = require('path')
const istanbulCoverage = require('istanbul-lib-coverage')
const { createReporter } = require('istanbul-api')

const map = istanbulCoverage.createCoverageMap()
const reporter = createReporter()
const coveragePath = path.join(__dirname, '../coverage')
const runners = ['unit', 'stack', 'e2e']

runners.forEach(runner => {
  const coverage = require(path.join(coveragePath, runner, 'coverage-final.json'))
  Object.keys(coverage).forEach(filename => map.addFileCoverage(coverage[filename]))
})

reporter.addAll(['json', 'lcov', 'text', 'html'])
reporter.write(map)
