const globby = require('globby')
const { build } = require('estrella')

globby(['src/**/*.js', 'src/**/*.ts']).then(files => {
  build({
    watch: process.argv[2] === '--watch',
    entry: files,
    outdir: '.build/src',
    platform: 'node',
    target: 'node14',
    minify: true,
    tslint: true,
    bundle: true,
    sourcemap: true,
  })
})
