# ton

- API para contar número de acessos: [https://79cnldb4i0.execute-api.us-east-2.amazonaws.com](https://79cnldb4i0.execute-api.us-east-2.amazonaws.com)

- API de autenticação: [https://wd5rvvjpcl.execute-api.us-east-2.amazonaws.com](https://wd5rvvjpcl.execute-api.us-east-2.amazonaws.com)

## Testando as apis

1. Obtenha a quantidade de visitas:

```bash
curl https://79cnldb4i0.execute-api.us-east-2.amazonaws.com/counter
```

2. Incremente o contador de visitas:

```bash
curl -X POST https://79cnldb4i0.execute-api.us-east-2.amazonaws.com/counter
```

3. Crie um usuário:

```bash
curl -X POST https://wd5rvvjpcl.execute-api.us-east-2.amazonaws.com/signup \
--header 'Content-Type: application/json' \
--data-raw '{
    "email": "youremail@example.com",
    "password": "Passwo000oord!",
    "name": "Your Name",
    "address": "somewhere far away"
}'
```

4. Abra o seu email e copie o código de verificação de 6 dígitos

5. Confirme o email passando o código de verificação:

```bash
curl -X POST https://wd5rvvjpcl.execute-api.us-east-2.amazonaws.com/signup/confirm \
--header 'Content-Type: application/json' \
--data-raw '{
    "email": "youremail@example.com",
    "code": "822961"
}'
```

6. Faça o login com o usuário criado e obtenha um token de autorização `idToken`:

```bash
curl -X POST https://wd5rvvjpcl.execute-api.us-east-2.amazonaws.com/login \
--header 'Content-Type: application/json' \
--data-raw '{
    "email": "youremail@example.com",
    "password": "Passwo000oord!"
}'
```

7. Use o idToken no header `Authorization` e visualize os dados do usuário logado:

```bash
curl https://wd5rvvjpcl.execute-api.us-east-2.amazonaws.com/user \
--header 'Authorization: Bearer eyJraWQiOiI1NzJieEdYa2RNSFBlQldKNGZtT1wvVFVOeHY1Wm93NHJIZ3RXYTFHNFhZaz0iLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJjMTNjMTE0Ny1jMzI5LTQyMDgtOTI2NS1lY2JlY2E1ZmZjNzEiLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwiYWRkcmVzcyI6eyJmb3JtYXR0ZWQiOiJzb21ld2hlcmUgZmFyIGF3YXkifSwiaXNzIjoiaHR0cHM6XC9cL2NvZ25pdG8taWRwLnVzLWVhc3QtMi5hbWF6b25hd3MuY29tXC91cy1lYXN0LTJfanFhTklyQkhaIiwiY29nbml0bzp1c2VybmFtZSI6ImMxM2MxMTQ3LWMzMjktNDIwOC05MjY1LWVjYmVjYTVmZmM3MSIsImF1ZCI6IjFobDcyZDBtcnU0Z2ZzNHBkbWo0MHRwcWdtIiwiZXZlbnRfaWQiOiIwMzkxMDZiZS1mYTMxLTQ0MGYtYTdlYS1jM2FjZTI2NmU1MDMiLCJ0b2tlbl91c2UiOiJpZCIsImF1dGhfdGltZSI6MTYyMDM3NDQ1MSwibmFtZSI6IllvdXIgTmFtZSIsImV4cCI6MTYyMDM3ODA1MSwiaWF0IjoxNjIwMzc0NDUxLCJlbWFpbCI6InNvdXNhLmRmc0BnbWFpbC5jb20ifQ.eWu8WbDrSCBIafBenqolfU7gD6dZs6lmgXAOeu0D02yGvJNNNX70IZgqlUkasH33d4QBxBFNhVGf9LAhxauuHFyZb1iMBPYAgO635DdAh3UPgIMMI31pp1sy_4lFuEnVb4fHYMbOI-6uTMlPvZGJ0xq4-7RFTTTi9Vb3ped_-W-AH0aSCMfr2ZlJHDE6RpDw-bKDes2Lvk1iU0Xoc-1vtS05aDegD7fYbAtw0lSVDW8vnJt_yw2dejyax2FrlYmt8Td7Pn4Kb01nk0JCUeZ1WJomrlaRp_QEu9y281Ra8LMYewgpQ-ntm2tI3hBU5ys9qAKujoGRdAxRePoqVD0qXQ'
```

## Documentação

As documentações seguindo a especificação do open-api de ambas as apis se encontram na pasta [docs](./docs):

- [openapi-counter.yaml](./docs/openapi-counter.yaml)
- [openapi-auth.yaml](./docs/openapi-auth.yaml)

É possível executar um servidor do swagger-ui para visualizar as documentações localmente através do seguinte comando:

```bash
npm run docs:api
```

## Arquitetura

Como não tive muito tempo para documentar a arquitetura, decidi gerar automaticamente o diagrama com os componentes de infraestrutura com o [@mhlabs/cfn-diagram](https://www.npmjs.com/package/@mhlabs/cfn-diagram).

Por ser gerado automaticamente o resultado não é tão bom, mas é melhor do que nada :)

Para gerar o diagrama:

```bash
npm run docs:diagram
```

![Auth Api](./docs/template.png)

## Estrutura de pastas

```bash
├── events  # mocks de eventos para depurar funções localmente
├── lib  # código de infraestruta da AWS
├── scripts  # scripts de desenvolvimento
├── src  # código referente às lambdas
│   ├── auth  # módulo de autenticação
│   ├── common  # arquivos comuns entre módulos
│   ├── counter  # módulo do contador
│   └── middlewares  # middy middlewares
└── test  # testes de integração / end-to-end
    ├── e2e  # testes end-to-end
```

## Executando localmente

Para executar as apis localmente é necessário ter instaldo o
[AWS Serverless Application Model](https://aws.amazon.com/serverless/sam/) e executar os seguintes comandos:

**Simular um evento**

```bash
npm run invoke -- PreSignUp -e events/pre-sign-up.json
```

**Iniciar api localmente**

```bash
npm start
```

## Linting e Testes

Para analisar o código com eslint e prettier:

```bash
npm run lint      # analisa código sem fazer alterações
npm run lint:fix  # analisa código e corrige problemas
```

Escrevi alguns exemplos de testes para o módulo do contador.

Antes de executar os testes de integração é necessário iniciar a api localmente:

```bash
npm start
```

Para executar os testes:

```bash
# Executa testes unitários, da stack, e de integração com cobertura de código
npm run test

# Executa testes unitários
npm run test:unit

# Executa testes unitários da infraestrutura (com aws-cdk)
npm run test:stack

# Executa testes de integração (api local)
npm run test:integration

# Execuata testes e2e (endpoint do apigateway no ambiente de desenvolvimento)
npm run test:e2s
```

## Git hooks

- `pre-commit`: Irá executar as ferramnetas para linting (eslint e prettier) e testes unitários (jest) para os arquivos que se encontram na staging area antes de cada commit.
- `commit-msg`: Irá validar a mensagem de cada commit seguindo o padrão do [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/#summary).

## Depuração

### Depurando localmente

Para depurar localmente execute um dos comandos abaixo e inicie o debugger na porta 5858:

#### Depurar um evento

```bash
npm run debug:invoke -- SignUp -e events/sign-up.json
```

#### Depurar as apis

```bash
npm run debug:api
```

### Depurando remotamente

Com o framework serverless stack é possível também depurar pelo vscode se conectando ao ambiente de desenvolvimento:

https://docs.serverless-stack.com/live-lambda-development

Para isso digite:

```bash
npm start:live
```

## Deploy

Para realizar o deploy digite:

```bash
npm run deploy -- [stack] --stage [dev|test|prod]
```

O parâmetro stage pode ser omitido caso o objetivo seja fazer o deploy no ambiente de desenvolvimento.
