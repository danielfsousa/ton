module.exports = {
  preset: 'ts-jest',
  coverageReporters: ['json'],
  verbose: true,
  coverageDirectory: 'coverage/unit',
  collectCoverageFrom: ['src/**', '!src/**/*.test.ts'],
  testPathIgnorePatterns: ['/node_modules/', './build', '/lib', '/test/integration', '/test/e2e'],
  testEnvironment: 'node',
  globals: {
    'ts-jest': {
      tsconfig: './tsconfig.json',
    },
  },
}
