import got from 'got'

export const API_URL = process.env.API_URL ?? 'http://127.0.0.1:3000'

export const api = got.extend({
  prefixUrl: API_URL,
  responseType: 'json',
})
