import { GetCounterResponseBody } from '../../src/counter/get-counter-fn'
import { IncrementCounterResponseBody } from '../../src/counter/increment-counter-fn'
import { api } from '../utils'

describe('POST /counter', () => {
  test('increments counter', async () => {
    const getResponse = await api.get<GetCounterResponseBody>('counter')
    const postResponse = await api.post<IncrementCounterResponseBody>('counter')

    expect(postResponse.statusCode).toBe(200)
    expect(postResponse.body.counter).toBe(getResponse.body.counter + 1)
  })
})
