import { GetCounterResponseBody } from '../../src/counter/get-counter-fn'
import { api } from '../utils'

describe('GET /counter', () => {
  test('gets counter', async () => {
    const res = await api.get<GetCounterResponseBody>('counter')

    expect(res.statusCode).toBe(200)
    expect(typeof res.body.counter).toBe('number')
  })
})
