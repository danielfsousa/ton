import middy from '@middy/core'
import httpErrorHandler from '@middy/http-error-handler'
import { APIGatewayProxyEventV2, APIGatewayProxyResultV2, Handler } from 'aws-lambda'
import createHttpError from 'http-errors'
import responseSerializer from '../middlewares/response-serializer'
import { incrementCounter } from './counter.service'

export interface IncrementCounterResponseBody {
  counter: number
}

export interface IncrementCounterResponse {
  body: IncrementCounterResponseBody
}

export type IncrementCounterHandler = Handler<
  APIGatewayProxyEventV2,
  APIGatewayProxyResultV2<IncrementCounterResponse>
>

const baseHandler: IncrementCounterHandler = async () => {
  const stage = process.env.STAGE
  if (!stage) {
    throw createHttpError(500, 'STAGE environment not defined', { expose: false })
  }

  const counter = await incrementCounter(stage)
  return {
    statusCode: 200,
    body: { counter },
  }
}

export const handler = middy(baseHandler) //
  .use(responseSerializer())
  .use(httpErrorHandler())
