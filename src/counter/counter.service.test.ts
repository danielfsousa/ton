import countApi from 'countapi-js'
import createHttpError from 'http-errors'
import { getCounter, incrementCounter, COUNTER_NAMESPACE, COUNTER_KEY } from './counter.service'

const STAGE = 'dev'
const KEY = `${COUNTER_KEY}-${STAGE}`

jest.mock('countapi-js')

describe('getCounter()', () => {
  test('returns counter', async () => {
    const fakeResponse = {
      path: '',
      status: 200,
      value: 100,
    }

    const mockGet = jest.spyOn(countApi, 'get')
    mockGet.mockResolvedValueOnce(fakeResponse)
    const counter = await getCounter(STAGE)

    expect(mockGet).toBeCalledWith(COUNTER_NAMESPACE, KEY)
    expect(counter).toBe(fakeResponse.value)
  })

  test('throws error when status is different than 200', async () => {
    const fakeResponse = {
      path: '',
      status: 404,
      value: 100,
    }

    const expectedError = createHttpError(fakeResponse.status)

    const mockGet = jest.spyOn(countApi, 'get')
    mockGet.mockResolvedValueOnce(fakeResponse)
    const counterPromise = getCounter(STAGE)

    await expect(counterPromise).rejects.toEqual(expectedError)
    expect(mockGet).toBeCalledWith(COUNTER_NAMESPACE, KEY)
  })
})

describe('incrementCounter()', () => {
  test('increments counter', async () => {
    const fakeResponse = {
      path: '',
      status: 200,
      value: 100,
    }

    const mockHit = jest.spyOn(countApi, 'hit')
    mockHit.mockResolvedValueOnce(fakeResponse)
    const counter = await incrementCounter(STAGE)

    expect(mockHit).toBeCalledWith(COUNTER_NAMESPACE, KEY)
    expect(counter).toBe(fakeResponse.value)
  })

  test('throws error when status is different than 200', async () => {
    const fakeResponse = {
      path: '',
      status: 404,
      value: 100,
    }

    const expectedError = createHttpError(fakeResponse.status)

    const mockHit = jest.spyOn(countApi, 'hit')
    mockHit.mockResolvedValueOnce(fakeResponse)
    const counterPromise = incrementCounter(STAGE)

    await expect(counterPromise).rejects.toEqual(expectedError)
    expect(mockHit).toBeCalledWith(COUNTER_NAMESPACE, KEY)
  })
})
