import middy from '@middy/core'
import httpErrorHandler from '@middy/http-error-handler'
import { APIGatewayProxyEventV2, APIGatewayProxyResultV2, Handler } from 'aws-lambda'
import createHttpError from 'http-errors'
import responseSerializer from '../middlewares/response-serializer'
import { getCounter } from './counter.service'

export interface GetCounterResponseBody {
  counter: number
}

export interface GetCounterResponse {
  body: GetCounterResponseBody
}

export type GetCounterHandler = Handler<
  APIGatewayProxyEventV2,
  APIGatewayProxyResultV2<GetCounterResponse>
>

const baseHandler: GetCounterHandler = async () => {
  const stage = process.env.STAGE
  if (!stage) {
    throw createHttpError(500, 'STAGE environment not defined', { expose: false })
  }

  const counter = await getCounter(stage)
  return {
    statusCode: 200,
    body: { counter },
  }
}

export const handler = middy(baseHandler) //
  .use(responseSerializer())
  .use(httpErrorHandler())
