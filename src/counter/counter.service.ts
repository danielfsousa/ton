import { get, hit } from 'countapi-js'
import createHttpError from 'http-errors'

export const COUNTER_NAMESPACE = 'ton.stone.com.br'
export const COUNTER_KEY = 'visits'

function getCounterKey(stage: string): string {
  return `${COUNTER_KEY}-${stage}`
}

export async function getCounter(stage: string): Promise<number> {
  const { value, status } = await get(COUNTER_NAMESPACE, getCounterKey(stage))
  if (status !== 200) {
    throw createHttpError(status)
  }

  return value
}

export async function incrementCounter(stage: string): Promise<number> {
  const { value, status } = await hit(COUNTER_NAMESPACE, getCounterKey(stage))
  if (status !== 200) {
    throw createHttpError(status)
  }

  return value
}
