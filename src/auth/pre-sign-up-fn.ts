import { AttributeValue, DynamoDBClient, PutItemCommand } from '@aws-sdk/client-dynamodb'
import middy from '@middy/core'
import errorLogger from '@middy/error-logger'
import { PreSignUpTriggerHandler } from 'aws-lambda'

const client = new DynamoDBClient({})

const baseHandler: PreSignUpTriggerHandler = async event => {
  const { USERS_TABLE_NAME } = process.env

  const user = event.request.userAttributes
  const item: { [key: string]: AttributeValue } = {
    id: { S: event.userName },
    email: { S: user.email },
    name: { S: user.name },
    createdAt: { S: new Date().toISOString() },
  }

  if ('address' in user) {
    item.address = { S: user.address }
  }

  const command = new PutItemCommand({
    TableName: USERS_TABLE_NAME,
    Item: item,
  })

  await client.send(command)

  return event
}

export const handler = middy(baseHandler).use(errorLogger())
