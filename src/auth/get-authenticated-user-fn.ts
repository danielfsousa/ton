import { DynamoDBClient, GetItemCommand } from '@aws-sdk/client-dynamodb'
import middy from '@middy/core'
import httpErrorHandler from '@middy/http-error-handler'
import jsonBodyParser from '@middy/http-json-body-parser'
import { Handler, APIGatewayProxyResultV2, APIGatewayProxyEventV2 } from 'aws-lambda'
import createHttpError from 'http-errors'
import responseSerializer from '../middlewares/response-serializer'

export interface GetAuthenticatedUserResponse {
  body: {
    id: string
    email: string
    name: string
    address: string
    createdAt: string
    updatedAt: string
  }
}

export type GetAuthenticatedUserHandler = Handler<
  APIGatewayProxyEventV2,
  APIGatewayProxyResultV2<GetAuthenticatedUserResponse>
>

const client = new DynamoDBClient({})

const baseHandler: GetAuthenticatedUserHandler = async event => {
  const { USERS_TABLE_NAME } = process.env

  const auth = event.requestContext.authorizer?.jwt.claims
  if (typeof auth?.sub !== 'string') {
    throw createHttpError(403, 'Invalid credentials')
  }

  const command = new GetItemCommand({
    TableName: USERS_TABLE_NAME,
    Key: {
      id: { S: auth.sub },
    },
  })

  const results = await client.send(command)
  if (!results.Item) {
    throw createHttpError(404, 'User not found')
  }

  return {
    statusCode: 200,
    body: {
      id: results.Item.id.S as string,
      email: results.Item.email.S as string,
      name: results.Item.name.S as string,
      address: results.Item.address?.S as string,
      createdAt: results.Item.createdAt.S as string,
      updatedAt: results.Item.updatedAt?.S as string,
    },
  }
}

export const handler = middy(baseHandler)
  .use(jsonBodyParser())
  .use(responseSerializer())
  .use(httpErrorHandler())
