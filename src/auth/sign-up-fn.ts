import { APIGatewayProxyEventV2, APIGatewayProxyResultV2, Handler } from 'aws-lambda'
import { CognitoUserPool } from 'amazon-cognito-identity-js'
import { JSONSchemaType } from 'ajv'
import middy from '@middy/core'
import httpErrorHandler from '@middy/http-error-handler'
import jsonBodyParser from '@middy/http-json-body-parser'
import validator from '@middy/validator'
import createHttpError from 'http-errors'
import responseSerializer from '../middlewares/response-serializer'
import { signUp, SignUpAttributes } from './auth.service'

export interface SignUpRequest {
  body: SignUpAttributes
}

export interface SignUpResponse {
  body: {
    id: string
    email: string
    name: string
    address?: string
    userConfirmed: boolean
  }
}

export type SignUpEvent = Omit<APIGatewayProxyEventV2, 'body'> & SignUpRequest

export type SignUpHandler = Handler<SignUpEvent, APIGatewayProxyResultV2<SignUpResponse>>

export const baseHandler: SignUpHandler = async event => {
  const { COGNITO_USER_POOL_ID, COGNITO_CLIENT_ID } = process.env
  const { body } = event

  if (!COGNITO_USER_POOL_ID || !COGNITO_CLIENT_ID) {
    throw createHttpError(500, 'Missing cognito variables', { expose: false })
  }

  try {
    const result = await signUp({
      attributes: body,
      userPool: new CognitoUserPool({
        UserPoolId: COGNITO_USER_POOL_ID,
        ClientId: COGNITO_CLIENT_ID,
      }),
    })

    return {
      statusCode: 201,
      body: {
        id: result.userSub,
        email: result.user.getUsername(),
        name: body.name,
        address: body.address,
        userConfirmed: result.userConfirmed,
      },
    }
  } catch (err) {
    switch (err.code) {
      case 'UsernameExistsException':
        throw createHttpError(409, err)
      case 'InvalidPasswordException':
        throw createHttpError(400, err)
      default:
        throw err
    }
  }
}

// This validation will cause a 50-100ms performance hit during cold start.
// Precompiling schemas is recommended.
export const inputSchema: JSONSchemaType<SignUpRequest> = {
  type: 'object',
  required: ['body'],
  properties: {
    body: {
      type: 'object',
      required: ['email', 'password', 'name'],
      properties: {
        email: { type: 'string', format: 'email', maxLength: 320 },
        password: { type: 'string', minLength: 8, maxLength: 320 },
        name: { type: 'string', maxLength: 320 },
        address: { type: 'string', nullable: true, maxLength: 320 },
      },
    },
  },
}

export const handler = middy(baseHandler)
  .use(jsonBodyParser())
  .use(validator({ inputSchema }))
  .use(responseSerializer())
  .use(httpErrorHandler())
