import { APIGatewayProxyEventV2, APIGatewayProxyResultV2, Handler } from 'aws-lambda'
import createHttpError from 'http-errors'
import middy from '@middy/core'
import httpErrorHandler from '@middy/http-error-handler'
import jsonBodyParser from '@middy/http-json-body-parser'
import validator from '@middy/validator'
import responseSerializer from '../middlewares/response-serializer'
import { JSONSchemaType } from 'ajv'
import { confirmRegistrarion } from './auth.service'

export interface SignUpConfirmRequest {
  body: {
    email: string
    code: string
  }
}

export interface SignUpConfirmResponse {
  body: {
    message: string
  }
}

export type SignUpConfirmEvent = Omit<APIGatewayProxyEventV2, 'body'> & SignUpConfirmRequest

export type SignUpConfirmHandler = Handler<
  SignUpConfirmEvent,
  APIGatewayProxyResultV2<SignUpConfirmResponse>
>

const baseHandler: SignUpConfirmHandler = async event => {
  const { COGNITO_USER_POOL_ID, COGNITO_CLIENT_ID } = process.env
  const { body } = event

  if (!COGNITO_USER_POOL_ID || !COGNITO_CLIENT_ID) {
    throw createHttpError(500, 'Missing cognito variables', { expose: false })
  }

  try {
    await confirmRegistrarion({
      userPoolId: COGNITO_USER_POOL_ID,
      clientId: COGNITO_CLIENT_ID,
      email: body.email,
      code: body.code,
    })
    return {
      statusCode: 200,
      body: { message: 'success' },
    }
  } catch (err) {
    switch (err.code) {
      case 'CodeMismatchException':
        throw createHttpError(400, err)
      default:
        throw err
    }
  }
}

export const inputSchema: JSONSchemaType<SignUpConfirmRequest> = {
  type: 'object',
  required: ['body'],
  properties: {
    body: {
      type: 'object',
      required: ['email', 'code'],
      properties: {
        email: { type: 'string', format: 'email', maxLength: 320 },
        code: { type: 'string', maxLength: 10 },
      },
    },
  },
}

export const handler = middy(baseHandler)
  .use(jsonBodyParser())
  .use(validator({ inputSchema }))
  .use(responseSerializer())
  .use(httpErrorHandler())
