import {
  CognitoUser,
  AuthenticationDetails,
  CognitoUserPool,
  ISignUpResult,
  CognitoUserAttribute,
} from 'amazon-cognito-identity-js'

export interface SignUpAttributes {
  email: string
  password: string
  name: string
  address?: string
}

export interface SignUpOptions {
  userPool: CognitoUserPool
  attributes: SignUpAttributes
}

export interface SignUpConfirmOptions {
  userPoolId: string
  clientId: string
  email: string
  code: string
}

export interface SignUpConfirmResult {
  tokenType: string
}

export interface AuthenticateOptions {
  userPoolId: string
  clientId: string
  email: string
  password: string
}

export interface AuthenticateResult {
  tokenType: string
  idToken: string
  accessToken: string
  refreshToken: string
  expiresIn: number
}

export async function signUp({
  userPool,
  attributes: { email, name, password, address },
}: SignUpOptions): Promise<ISignUpResult> {
  const cognitoAttributes = [new CognitoUserAttribute({ Name: 'name', Value: name })]

  if (address) {
    cognitoAttributes.push(new CognitoUserAttribute({ Name: 'address', Value: address }))
  }

  return await new Promise((resolve, reject) => {
    userPool.signUp(email, password, cognitoAttributes, [], (err, result) => {
      if (err) return reject(err)
      if (!result) return reject(new Error('Empty result'))
      resolve(result)
    })
  })
}

export async function confirmRegistrarion({
  userPoolId,
  clientId,
  email,
  code,
}: SignUpConfirmOptions): Promise<string> {
  const userPool = new CognitoUserPool({ UserPoolId: userPoolId, ClientId: clientId })
  const cognitoUser = new CognitoUser({ Username: email, Pool: userPool })
  const forceAliasCreation = true

  return await new Promise((resolve, reject) => {
    cognitoUser.confirmRegistration(code, forceAliasCreation, (err, result) => {
      if (err) return reject(err)
      resolve(result)
    })
  })
}

export async function authenticateUser({
  userPoolId,
  clientId,
  email,
  password,
}: AuthenticateOptions): Promise<AuthenticateResult> {
  const userPool = new CognitoUserPool({ UserPoolId: userPoolId, ClientId: clientId })
  const authenticationDetails = new AuthenticationDetails({ Username: email, Password: password })
  const cognitoUser = new CognitoUser({ Username: email, Pool: userPool })

  return await new Promise((resolve, reject) => {
    cognitoUser.authenticateUser(authenticationDetails, {
      onSuccess(result) {
        resolve({
          tokenType: 'Bearer',
          idToken: result.getIdToken().getJwtToken(),
          accessToken: result.getAccessToken().getJwtToken(),
          refreshToken: result.getRefreshToken().getToken(),
          expiresIn: result.getIdToken().getExpiration(),
        })
      },
      onFailure(err) {
        reject(err)
      },
    })
  })
}
