import middy from '@middy/core'
import httpErrorHandler from '@middy/http-error-handler'
import jsonBodyParser from '@middy/http-json-body-parser'
import validator from '@middy/validator'
import createHttpError from 'http-errors'
import { JSONSchemaType } from 'ajv'
import { APIGatewayProxyEventV2, APIGatewayProxyResultV2, Handler } from 'aws-lambda'
import responseSerializer from '../middlewares/response-serializer'
import { AuthenticateResult, authenticateUser } from './auth.service'

export interface LoginRequest {
  body: {
    email: string
    password: string
  }
}

export interface LoginResponse {
  body: AuthenticateResult
}

export type LoginEvent = Omit<APIGatewayProxyEventV2, 'body'> & LoginRequest

export type LoginHandler = Handler<LoginEvent, APIGatewayProxyResultV2<LoginResponse>>

const baseHandler: LoginHandler = async event => {
  const { COGNITO_USER_POOL_ID, COGNITO_CLIENT_ID } = process.env
  const { body } = event

  if (!COGNITO_USER_POOL_ID || !COGNITO_CLIENT_ID) {
    throw createHttpError(500, 'Missing cognito variables', { expose: false })
  }

  try {
    const result = await authenticateUser({
      email: body.email,
      password: body.password,
      userPoolId: COGNITO_USER_POOL_ID,
      clientId: COGNITO_CLIENT_ID,
    })
    return { statusCode: 200, body: result }
  } catch (err) {
    switch (err.code) {
      case 'UserNotConfirmedException':
        throw createHttpError(403, err)
      default:
        throw err
    }
  }
}

export const inputSchema: JSONSchemaType<LoginRequest> = {
  type: 'object',
  required: ['body'],
  properties: {
    body: {
      type: 'object',
      required: ['email', 'password'],
      properties: {
        email: { type: 'string', format: 'email', maxLength: 320 },
        password: { type: 'string', minLength: 8, maxLength: 320 },
      },
    },
  },
}

export const handler = middy(baseHandler)
  .use(jsonBodyParser())
  .use(validator({ inputSchema }))
  .use(responseSerializer())
  .use(httpErrorHandler())
