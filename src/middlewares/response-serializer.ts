import middy from '@middy/core'
import httpResponseSerializer from '@middy/http-response-serializer'

const responseSerializer: () => middy.MiddlewareObj = () =>
  httpResponseSerializer({
    default: 'application/json',
    serializers: [
      {
        regex: /^application\/json$/,
        serializer: ({ body }) =>
          typeof body === 'string' ? JSON.stringify({ message: body }) : JSON.stringify(body),
      },
    ],
  })

export default responseSerializer
